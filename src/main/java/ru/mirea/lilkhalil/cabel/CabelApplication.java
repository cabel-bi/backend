package ru.mirea.lilkhalil.cabel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CabelApplication {

	public static void main(String[] args) {
		SpringApplication.run(CabelApplication.class, args);
	}

}
