package ru.mirea.lilkhalil.cabel.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Component;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@Component
public class CalculationServiceRegistry {
    Map<String, CalculationService> calculationServices = new HashMap<>();

    public void register(String type, CalculationService calculationService) {
        calculationServices.put(type, calculationService);
    }

    public CalculationService get(String type) {
        return Objects.requireNonNull(calculationServices.get(type));
    }
}
