package ru.mirea.lilkhalil.cabel.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Component;
import ru.mirea.lilkhalil.cabel.service.AnalysisService;

@Component
public class AnalysisServiceRegistry {

    Map<String, AnalysisService> analysisServices = new HashMap<>();

    public void register(String type, AnalysisService analysisService) {
        analysisServices.put(type, analysisService);
    }

    public AnalysisService get(String type) {
        return Objects.requireNonNull(analysisServices.get(type));
    }
}
