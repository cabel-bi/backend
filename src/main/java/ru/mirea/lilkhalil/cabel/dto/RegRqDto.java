package ru.mirea.lilkhalil.cabel.dto;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RegRqDto {
    private String firstName;
    private String lastName;
    private String email;
    private JobDto job;
    private List<RoleDto> roles;
}
