package ru.mirea.lilkhalil.cabel.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ReportDto {
    private String name;
    private String description;
    private LocalDateTime creationDate;
    private String type;
    private String objectId;
}
