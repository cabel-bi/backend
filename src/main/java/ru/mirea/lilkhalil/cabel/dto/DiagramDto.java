package ru.mirea.lilkhalil.cabel.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DiagramDto {
    private String name;
    private String type;
    private LocalDateTime creationDate;
    private String objectId;
}
