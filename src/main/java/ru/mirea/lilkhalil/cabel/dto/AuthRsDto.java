package ru.mirea.lilkhalil.cabel.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AuthRsDto {
    String accessToken;
    String refreshToken;
}
