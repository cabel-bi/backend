package ru.mirea.lilkhalil.cabel.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;

@Data
@Accessors(chain = true)
public class ErrorDto {
    private HttpStatus status;
    private String message;
}
