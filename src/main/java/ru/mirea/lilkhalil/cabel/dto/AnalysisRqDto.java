package ru.mirea.lilkhalil.cabel.dto;

import java.time.LocalDate;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AnalysisRqDto {
    private String type;
    private LocalDate startDate;
    private LocalDate endDate;
}
