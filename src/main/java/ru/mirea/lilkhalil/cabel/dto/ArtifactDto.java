package ru.mirea.lilkhalil.cabel.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ArtifactDto {
    private Long id;
    private String type;
    private String objectId;
    private String name;
    private LocalDateTime creationDate;
    private UserDto userDto;
}
