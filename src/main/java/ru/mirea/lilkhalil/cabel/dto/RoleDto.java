package ru.mirea.lilkhalil.cabel.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RoleDto {
    private Integer id;
    private String name;
}
