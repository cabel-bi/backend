package ru.mirea.lilkhalil.cabel.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.mirea.lilkhalil.cabel.domain.Category;
import ru.mirea.lilkhalil.cabel.domain.Company;
import ru.mirea.lilkhalil.cabel.domain.Employee;

@Data
@Accessors(chain = true)
public class CalculationRqDto {
    private String type;
    private List<Company> companies = new ArrayList<>();
    private List<Category> categories = new ArrayList<>();
    private List<Employee> employees = new ArrayList<>();
    private LocalDate startDate;
    private LocalDate endDate;
}
