package ru.mirea.lilkhalil.cabel.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ChartDto {
    private LocalDate startPeriod;
    private LocalDate endPeriod;
    private LocalDateTime creationDate;
    private String name;
    private String type;
    private String objectId;
}
