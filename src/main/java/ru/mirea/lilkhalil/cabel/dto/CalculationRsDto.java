package ru.mirea.lilkhalil.cabel.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CalculationRsDto {
    Object label;
    Number data;
}
