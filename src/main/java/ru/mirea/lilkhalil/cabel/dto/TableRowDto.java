package ru.mirea.lilkhalil.cabel.dto;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TableRowDto {
    String title;
    BigDecimal sales;
    BigDecimal share;
    BigDecimal cumulativeShare;
    String abc;
    String xyz;
}
