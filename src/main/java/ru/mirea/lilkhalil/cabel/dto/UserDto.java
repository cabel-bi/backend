package ru.mirea.lilkhalil.cabel.dto;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDto {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String job;
    private List<String> roles;
}
