package ru.mirea.lilkhalil.cabel.service.impl;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.dto.AnalysisRqDto;
import ru.mirea.lilkhalil.cabel.dto.TableRowDto;
import ru.mirea.lilkhalil.cabel.registry.AnalysisServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;
import ru.mirea.lilkhalil.cabel.service.AnalysisService;

@Service
@RequiredArgsConstructor
public class CompanyAnalysisService implements AnalysisService {

    private final DealRepository dealRepository;

    @Override
    public List<TableRowDto> perform(AnalysisRqDto analysisRqDto) {
        List<Deal> deals = dealRepository.findByCreationDateGreaterThanEqualAndClosingDateLessThanEqual(
                analysisRqDto.getStartDate().atStartOfDay(),
                analysisRqDto.getEndDate().atStartOfDay()
        );
        return this.perform(deals, deal -> deal.getDeal().getContact().getCompany());
    }

    @Override
    @Autowired
    public void register(AnalysisServiceRegistry analysisServiceRegistry) {
        analysisServiceRegistry.register("Компания", this);
    }
}
