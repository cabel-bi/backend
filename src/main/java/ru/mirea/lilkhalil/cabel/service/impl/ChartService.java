package ru.mirea.lilkhalil.cabel.service.impl;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Chart;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;
import ru.mirea.lilkhalil.cabel.dto.ChartDto;
import ru.mirea.lilkhalil.cabel.mapper.ArtifactMapper;
import ru.mirea.lilkhalil.cabel.repository.ChartRepository;
import ru.mirea.lilkhalil.cabel.service.ArtifactService;

@Service
@RequiredArgsConstructor
public class ChartService implements ArtifactService {

    private final ChartRepository chartRepository;
    private final ArtifactMapper artifactMapper;

    public void createChart(ChartDto chartDto, User user) {
        Chart chart = artifactMapper.chartDtoToChart(chartDto);
        chart.setUser(user);
        chartRepository.save(chart);
    }

    public void deleteById(Long id) {
        chartRepository.deleteById(id);
    }

    @Override
    public List<ArtifactDto> findAll() {
        return chartRepository.findAll().stream()
                .map(artifactMapper::chartToArtifactDto)
                .toList();
    }
}
