package ru.mirea.lilkhalil.cabel.service;

public interface NotificationService {
    void send(String sendTo, char[] password);
}
