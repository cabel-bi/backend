package ru.mirea.lilkhalil.cabel.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.Employee;
import ru.mirea.lilkhalil.cabel.domain.join.DealProduct;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@Service
@RequiredArgsConstructor
public class ProductiveEmployeesCalculationService implements CalculationService {

    private final DealRepository dealRepository;

    @Override
    public List<CalculationRsDto> calculate(CalculationRqDto calculationRqDto) {
        List<Deal> deals = this.filterDeals(dealRepository, calculationRqDto);

        Map<Employee, BigDecimal> employeeSales = deals.stream()
                .flatMap(deal -> deal.getProducts().stream())
                .collect(Collectors.groupingBy(dealProduct -> dealProduct.getDeal().getEmployee(),
                        Collectors.reducing(BigDecimal.ZERO, DealProduct::getTotal, BigDecimal::add)));

        List<Map.Entry<Employee, BigDecimal>> bestEmployees = employeeSales.entrySet().stream()
                .sorted(Map.Entry.<Employee, BigDecimal>comparingByValue().reversed())
                .limit(10)
                .toList();

        return bestEmployees.stream()
                .map(entry -> new CalculationRsDto()
                        .setLabel(entry.getKey().getFirstName() + " " + entry.getKey().getLastName())
                        .setData(entry.getValue()))
                .toList();
    }

    @Override
    @Autowired
    public void register(CalculationServiceRegistry serviceRegistry) {
        serviceRegistry.register("Продуктивные менеджеры", this);
    }
}
