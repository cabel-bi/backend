package ru.mirea.lilkhalil.cabel.service.impl;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Report;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;
import ru.mirea.lilkhalil.cabel.dto.ReportDto;
import ru.mirea.lilkhalil.cabel.mapper.ArtifactMapper;
import ru.mirea.lilkhalil.cabel.repository.ReportRepository;
import ru.mirea.lilkhalil.cabel.service.ArtifactService;

@Service
@RequiredArgsConstructor
public class ReportService implements ArtifactService {

    private final ReportRepository reportRepository;
    private final ArtifactMapper artifactMapper;

    public void createReport(ReportDto reportDto, User user) {
        Report report = artifactMapper.reportDtoToReport(reportDto);
        report.setUser(user);
        reportRepository.save(report);
    }

    public void deleteById(Long id) {
        reportRepository.deleteById(id);
    }

    @Override
    public List<ArtifactDto> findAll() {
        return reportRepository.findAll().stream()
                .map(artifactMapper::reportToArtifactDto)
                .toList();
    }
}
