package ru.mirea.lilkhalil.cabel.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.join.DealProduct;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@Service
@RequiredArgsConstructor
public class AverageSalesCalculationService implements CalculationService {

    private final DealRepository dealRepository;

    @Override
    public List<CalculationRsDto> calculate(CalculationRqDto calculationRqDto) {
        List<Deal> deals = this.filterDeals(dealRepository, calculationRqDto);

        Map<LocalDate, BigDecimal> totalSales = deals.stream()
                .collect(Collectors.groupingBy(deal -> deal.getCreationDate().toLocalDate(),
                        Collectors.mapping(deal -> deal.getProducts().stream()
                                .map(DealProduct::getTotal)
                                .reduce(BigDecimal.ZERO, BigDecimal::add), Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));

        Map<LocalDate, BigDecimal> averageSales = totalSales.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().divide(BigDecimal.valueOf(entry.getKey().lengthOfMonth()), 2, RoundingMode.HALF_UP)));

        return averageSales.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(entry -> new CalculationRsDto()
                        .setLabel(entry.getKey())
                        .setData(entry.getValue()))
                .toList();
    }

    @Override
    @Autowired
    public void register(CalculationServiceRegistry serviceRegistry) {
        serviceRegistry.register("Средний чек", this);
    }
}
