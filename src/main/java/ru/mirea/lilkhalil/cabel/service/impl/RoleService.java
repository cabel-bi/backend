package ru.mirea.lilkhalil.cabel.service.impl;

import java.util.Collection;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.dto.RoleDto;
import ru.mirea.lilkhalil.cabel.mapper.UserMapper;
import ru.mirea.lilkhalil.cabel.repository.RoleRepository;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;
    private final UserMapper userMapper;

    public Collection<RoleDto> findAll() {
        return roleRepository.findAll().stream()
                .map(userMapper::roleToRoleDto)
                .toList();
    }
}
