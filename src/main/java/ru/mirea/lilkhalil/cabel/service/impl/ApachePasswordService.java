package ru.mirea.lilkhalil.cabel.service.impl;

import org.apache.commons.text.RandomStringGenerator;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.service.PasswordService;

@Service
public class ApachePasswordService implements PasswordService {
    @Override
    public char[] generatePassword(int length) {
        RandomStringGenerator passwordGenerator = RandomStringGenerator.builder()
                .withinRange(33, 126)
                .get();
        return passwordGenerator.generate(length).toCharArray();
    }
}
