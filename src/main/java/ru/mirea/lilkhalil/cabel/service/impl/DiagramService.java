package ru.mirea.lilkhalil.cabel.service.impl;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Diagram;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;
import ru.mirea.lilkhalil.cabel.dto.DiagramDto;
import ru.mirea.lilkhalil.cabel.mapper.ArtifactMapper;
import ru.mirea.lilkhalil.cabel.repository.DiagramRepository;
import ru.mirea.lilkhalil.cabel.service.ArtifactService;

@Service
@RequiredArgsConstructor
public class DiagramService implements ArtifactService {

    private final DiagramRepository diagramRepository;
    private final ArtifactMapper artifactMapper;

    public void createDiagram(DiagramDto diagramDto, User user) {
        Diagram diagram = artifactMapper.diagramDtoToDiagram(diagramDto);
        diagram.setUser(user);
        diagramRepository.save(diagram);
    }

    public void deleteById(Long id) {
        diagramRepository.deleteById(id);
    }

    @Override
    public List<ArtifactDto> findAll() {
        return diagramRepository.findAll().stream()
                .map(artifactMapper::diagramToArtifactDto)
                .toList();
    }
}
