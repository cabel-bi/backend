package ru.mirea.lilkhalil.cabel.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.join.DealProduct;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@Service
@RequiredArgsConstructor
public class SalesByDayCalculationService implements CalculationService {

    private final DealRepository dealRepository;

    @Override
    public List<CalculationRsDto> calculate(CalculationRqDto calculationRqDto) {
        List<Deal> deals = this.filterDeals(dealRepository, calculationRqDto);

        Map<LocalDate, BigDecimal> salesByDay = deals.stream()
                .flatMap(deal -> deal.getCreationDate().toLocalDate().datesUntil(deal.getClosingDate().toLocalDate().plusDays(1)))
                .collect(Collectors.groupingBy(date -> date, Collectors.mapping(
                        date -> deals.stream()
                                .filter(deal -> !date.isBefore(deal.getCreationDate().toLocalDate()) && !date.isAfter(deal.getClosingDate().toLocalDate()))
                                .map(deal -> deal.getProducts().stream()
                                        .map(DealProduct::getTotal)
                                        .reduce(BigDecimal.ZERO, BigDecimal::add))
                                .reduce(BigDecimal.ZERO, BigDecimal::add),
                        Collectors.reducing(BigDecimal.ZERO, BigDecimal::add)
                )));

        return salesByDay.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(entry -> new CalculationRsDto()
                        .setLabel(entry.getKey())
                        .setData(entry.getValue()))
                .toList();
    }

    @Override
    @Autowired
    public void register(CalculationServiceRegistry serviceRegistry) {
        serviceRegistry.register("Динамика продаж", this);
    }
}
