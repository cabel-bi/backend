package ru.mirea.lilkhalil.cabel.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.join.DealProduct;
import ru.mirea.lilkhalil.cabel.dto.AnalysisRqDto;
import ru.mirea.lilkhalil.cabel.dto.TableRowDto;
import ru.mirea.lilkhalil.cabel.registry.AnalysisServiceRegistry;

public interface AnalysisService {
    List<TableRowDto> perform(AnalysisRqDto analysisRqDto);
    void register(AnalysisServiceRegistry analysisServiceRegistry);

    default <T> List<TableRowDto> perform(List<Deal> deals, Function<DealProduct, T> function) {
        List<Map.Entry<T, BigDecimal>> sales = deals.stream()
                .flatMap(deal -> deal.getProducts().stream())
                .collect(Collectors.groupingBy(
                        function,
                        Collectors.reducing(BigDecimal.ZERO, DealProduct::getTotal, BigDecimal::add)
                ))
                .entrySet().stream()
                .sorted(Map.Entry.<T, BigDecimal>comparingByValue().reversed())
                .limit(20)
                .toList();

        BigDecimal totalSales = sales.stream()
                .map(Map.Entry::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal cumulativeShare = BigDecimal.ZERO;
        List<TableRowDto> tableRowDtos = new ArrayList<>();

        for (Map.Entry<T, BigDecimal> entry : sales) {
            BigDecimal share = entry.getValue().divide(totalSales, 100, RoundingMode.HALF_UP);
            cumulativeShare = cumulativeShare.add(share);

            String abcCategory;
            if (cumulativeShare.compareTo(new BigDecimal("0.8")) <= 0) {
                abcCategory = "A";
            } else if (cumulativeShare.compareTo(new BigDecimal("0.95")) <= 0) {
                abcCategory = "B";
            } else {
                abcCategory = "C";
            }

            BigDecimal variation = calculateCoefficientOfVariation(deals.stream()
                    .flatMap(deal -> deal.getProducts().stream())
                    .filter(product -> Objects.equals(function.apply(product), entry.getKey()))
                    .map(DealProduct::getTotal)
                    .collect(Collectors.toList()));

            String xyzCategory;
            if (variation.compareTo(new BigDecimal("0.1")) <= 0) {
                xyzCategory = "X";
            } else if (variation.compareTo(new BigDecimal("0.25")) <= 0) {
                xyzCategory = "Y";
            } else {
                xyzCategory = "Z";
            }

            TableRowDto tableRowDto = new TableRowDto()
                    .setTitle(entry.getKey().toString())
                    .setSales(entry.getValue())
                    .setShare(share)
                    .setCumulativeShare(cumulativeShare)
                    .setAbc(abcCategory)
                    .setXyz(xyzCategory);
            tableRowDtos.add(tableRowDto);
        }
        return tableRowDtos;
    }

    private BigDecimal calculateCoefficientOfVariation(List<BigDecimal> sales) {
        BigDecimal mean = sales.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(sales.size()), RoundingMode.HALF_UP);
        BigDecimal variance = sales.stream()
                .map(sale -> sale.subtract(mean).pow(2))
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(sales.size()), RoundingMode.HALF_UP);
        BigDecimal standardDeviation = BigDecimal.valueOf(Math.sqrt(variance.doubleValue()));
        return standardDeviation.divide(mean, RoundingMode.HALF_UP);
    }
}
