package ru.mirea.lilkhalil.cabel.service.impl;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Objects;

import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class GridFSService {

    private final GridFsOperations gridFsOperations;

    public String uploadFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            String contentType = file.getContentType();
            ObjectId objectId = gridFsOperations
                    .store(file.getInputStream(), fileName, contentType);
            return objectId.toString();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public GridFsResource downloadFile(String objectId) {
        Query query = new Query(Criteria.where("_id").is(objectId));
        GridFSFile gridFSFile = gridFsOperations.findOne(query);
        Objects.requireNonNull(gridFSFile);
        return gridFsOperations.getResource(gridFSFile);
    }

    public void deleteFile(String objectId) {
        Query query = new Query(Criteria.where("_id").is(objectId));
        gridFsOperations.delete(query);
    }

}
