package ru.mirea.lilkhalil.cabel.service.impl;

import java.util.Collection;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.dto.JobDto;
import ru.mirea.lilkhalil.cabel.mapper.UserMapper;
import ru.mirea.lilkhalil.cabel.repository.JobRepository;

@Service
@RequiredArgsConstructor
public class JobService {

    private final JobRepository jobRepository;
    private final UserMapper userMapper;

    public Collection<JobDto> findAll() {
        return jobRepository.findAll().stream()
                .map(userMapper::jobToJobDto)
                .toList();
    }
}
