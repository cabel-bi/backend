package ru.mirea.lilkhalil.cabel.service;

public interface PasswordService {
    char[] generatePassword(int length);
}
