package ru.mirea.lilkhalil.cabel.service;

import java.time.LocalDateTime;
import java.util.List;

import ru.mirea.lilkhalil.cabel.domain.Category;
import ru.mirea.lilkhalil.cabel.domain.Company;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.Employee;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;

public interface CalculationService {

    List<CalculationRsDto> calculate(CalculationRqDto calculationRqDto);

    void register(CalculationServiceRegistry serviceRegistry);

    default List<Deal> filterDeals(DealRepository dealRepository, CalculationRqDto calculationRqDto) {
        LocalDateTime startDate = calculationRqDto.getStartDate().atStartOfDay();
        LocalDateTime endDate = calculationRqDto.getEndDate().atStartOfDay();
        List<Company> companies = calculationRqDto.getCompanies();
        List<Category> categories = calculationRqDto.getCategories();
        List<Employee> employees = calculationRqDto.getEmployees();

        return dealRepository.findByCreationDateGreaterThanEqualAndClosingDateLessThanEqual(startDate, endDate).stream()
                .filter(deal -> companies.isEmpty() || companies.contains(deal.getContact().getCompany()))
                .filter(deal -> categories.isEmpty() || deal.getProducts().stream()
                        .anyMatch(dp -> categories.contains(dp.getProduct().getCategory())))
                .filter(deal -> employees.isEmpty() || employees.contains(deal.getEmployee()))
                .toList();
    }
}
