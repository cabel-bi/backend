package ru.mirea.lilkhalil.cabel.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Category;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@Service
@RequiredArgsConstructor
public class CountByCategoryCalculationService implements CalculationService {

    private final DealRepository dealRepository;

    @Override
    public List<CalculationRsDto> calculate(CalculationRqDto calculationRqDto) {
        List<Deal> deals = this.filterDeals(dealRepository, calculationRqDto);

        Map<Category, Long> dealCountByCategory = deals.stream()
                .flatMap(deal -> deal.getProducts().stream())
                .collect(Collectors.groupingBy(
                        dealProduct -> dealProduct.getProduct().getCategory(),
                        Collectors.counting()
                ));

        return dealCountByCategory.entrySet().stream()
                .map(entry -> new CalculationRsDto()
                        .setLabel(entry.getKey().getName())
                        .setData(BigDecimal.valueOf(entry.getValue()))
                )
                .collect(Collectors.toList());
    }

    @Override
    @Autowired
    public void register(CalculationServiceRegistry serviceRegistry) {
        serviceRegistry.register("Количество сделок по товарам", this);
    }
}
