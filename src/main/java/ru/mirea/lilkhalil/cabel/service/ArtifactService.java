package ru.mirea.lilkhalil.cabel.service;

import java.util.List;

import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;

public interface ArtifactService {
    List<ArtifactDto> findAll();
}
