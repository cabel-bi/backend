package ru.mirea.lilkhalil.cabel.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.domain.Category;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.join.DealProduct;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.repository.DealRepository;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@Service
@RequiredArgsConstructor
public class CategoryShareCalculationService implements CalculationService {

    private final DealRepository dealRepository;

    @Override
    public List<CalculationRsDto> calculate(CalculationRqDto calculationRqDto) {
        List<Deal> deals = this.filterDeals(dealRepository, calculationRqDto);

        Map<Category, BigDecimal> categorySales = deals.stream()
                .flatMap(deal -> deal.getProducts().stream())
                .collect(Collectors.groupingBy(dealProduct -> dealProduct.getProduct().getCategory(),
                        Collectors.reducing(BigDecimal.ZERO, DealProduct::getTotal, BigDecimal::add)));

        BigDecimal totalSales = categorySales.values().stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return categorySales.entrySet().stream()
                .map(entry -> new CalculationRsDto()
                        .setLabel(entry.getKey().getName())
                        .setData(entry.getValue().divide(totalSales, 100, RoundingMode.HALF_UP)))
                .toList();
    }

    @Override
    @Autowired
    public void register(CalculationServiceRegistry serviceRegistry) {
        serviceRegistry.register("Доли товаров", this);
    }
}
