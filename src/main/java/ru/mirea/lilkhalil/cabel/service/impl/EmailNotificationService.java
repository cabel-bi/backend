package ru.mirea.lilkhalil.cabel.service.impl;

import java.nio.CharBuffer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.mirea.lilkhalil.cabel.service.NotificationService;

@Service
@RequiredArgsConstructor
public class EmailNotificationService implements NotificationService {

    private final JavaMailSender javaMailSender;
    private static final String template = """
            Вы были зарегистрированы в системе Cabel BI,
            
            Данные для авторизации в системе:
                email: %s,
                password: %s
            
            Вход в системе доступен по адресу %s.
            """;

    @Value("${frontend.hostname}/auth")
    private String hostname;

    @Override
    public void send(String sendTo, char[] password) {
        SimpleMailMessage message = new SimpleMailMessage();

        String messageBody = template.formatted(sendTo, CharBuffer.wrap(password).toString(), hostname);

        message.setTo(sendTo);
        message.setSubject("Cabel BI | Доступ к учётной записи");
        message.setText(messageBody);

        javaMailSender.send(message);
    }
}
