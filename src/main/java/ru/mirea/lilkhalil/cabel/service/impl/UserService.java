package ru.mirea.lilkhalil.cabel.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.domain.join.UserRole;
import ru.mirea.lilkhalil.cabel.domain.join.UserRoleId;
import ru.mirea.lilkhalil.cabel.dto.RegRqDto;
import ru.mirea.lilkhalil.cabel.dto.UserDto;
import ru.mirea.lilkhalil.cabel.exception.UserAlreadyExistsException;
import ru.mirea.lilkhalil.cabel.exception.UserNotFoundException;
import ru.mirea.lilkhalil.cabel.mapper.UserMapper;
import ru.mirea.lilkhalil.cabel.repository.UserRepository;
import ru.mirea.lilkhalil.cabel.service.NotificationService;
import ru.mirea.lilkhalil.cabel.service.PasswordService;

@Service
@RequiredArgsConstructor
public class UserService {

    private static final ZoneId zoneId = ZoneId.of("GMT+3");

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordService passwordService;
    private final NotificationService notificationService;
    private final ThreadPoolTaskExecutor taskExecutor;

    @Value("${password.length}")
    private int length;

    public UserDto findByEmail(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));
        return userMapper.userToUserDto(user);
    }

    public Collection<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(userMapper::userToUserDto)
                .toList();
    }

    @Transactional
    public UserDto createUser(RegRqDto requestBody) {

        if (userRepository.findByEmail(requestBody.getEmail()).isPresent()) {
            throw new UserAlreadyExistsException(requestBody.getEmail());
        }

        char[] password = passwordService.generatePassword(length);

        taskExecutor.execute(() -> notificationService.send(requestBody.getEmail(), password));

        User user = new User();

        user
            .setFirstName(requestBody.getFirstName())
            .setLastName(requestBody.getLastName())
            .setEmail(requestBody.getEmail())
            .setPassword(password)
            .setJob(userMapper.jobDtoToJob(requestBody.getJob()));

        userRepository.saveAndFlush(user);

        user.getRoles().addAll(requestBody.getRoles().stream()
                .map(userMapper::roleDtoToRole)
                .map(role -> new UserRole()
                        .setId(new UserRoleId()
                                .setUserId(user.getId())
                                .setRoleId(role.getId()))
                        .setUser(user)
                        .setRole(role)
                        .setIssueDate(LocalDateTime.now(zoneId)))
                .collect(Collectors.toSet()));

        return userMapper.userToUserDto(userRepository.save(user));
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
