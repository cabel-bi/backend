package ru.mirea.lilkhalil.cabel.mapper;

import java.util.List;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.mirea.lilkhalil.cabel.domain.Job;
import ru.mirea.lilkhalil.cabel.domain.Role;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.domain.join.UserRole;
import ru.mirea.lilkhalil.cabel.dto.JobDto;
import ru.mirea.lilkhalil.cabel.dto.RoleDto;
import ru.mirea.lilkhalil.cabel.dto.UserDto;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    @Mapping(source = "job.name", target = "job")
    UserDto userToUserDto(User user);
    Job jobDtoToJob(JobDto jobDto);

    JobDto jobToJobDto(Job job);

    Role roleDtoToRole(RoleDto roleDto);

    RoleDto roleToRoleDto(Role role);

    @Mapping(source = "roles", target = "roles", expression = "java(mapUserRolesToRoleNames(roles)")
    default List<String> mapUserRolesToRoleNames(Set<UserRole> roles) {
        return roles.stream()
                .map(UserRole::getRole)
                .map(Role::getName)
                .toList();
    }
}
