package ru.mirea.lilkhalil.cabel.mapper;

import lombok.Setter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mirea.lilkhalil.cabel.domain.Chart;
import ru.mirea.lilkhalil.cabel.domain.Diagram;
import ru.mirea.lilkhalil.cabel.domain.Report;
import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;
import ru.mirea.lilkhalil.cabel.dto.ChartDto;
import ru.mirea.lilkhalil.cabel.dto.DiagramDto;
import ru.mirea.lilkhalil.cabel.dto.ReportDto;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class ArtifactMapper {

    @Setter(onMethod_ = @Autowired)
    protected UserMapper userMapper;

    public abstract Chart chartDtoToChart(ChartDto chartDto);

    public abstract Report reportDtoToReport(ReportDto reportDto);

    public abstract Diagram diagramDtoToDiagram(DiagramDto diagramDto);

    @Mapping(target = "userDto", expression = "java(userMapper.userToUserDto(chart.getUser()))")
    public abstract ArtifactDto chartToArtifactDto(Chart chart);

    @Mapping(target = "userDto", expression = "java(userMapper.userToUserDto(report.getUser()))")
    public abstract ArtifactDto reportToArtifactDto(Report report);

    @Mapping(target = "userDto", expression = "java(userMapper.userToUserDto(diagram.getUser()))")
    public abstract ArtifactDto diagramToArtifactDto(Diagram diagram);
}
