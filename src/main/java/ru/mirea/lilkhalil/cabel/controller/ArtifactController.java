package ru.mirea.lilkhalil.cabel.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;
import ru.mirea.lilkhalil.cabel.service.ArtifactService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/artifacts")
public class ArtifactController {

    private final List<ArtifactService> artifactServices;

    @GetMapping
    public ResponseEntity<List<ArtifactDto>> getAll() {
        return ResponseEntity.ok()
                .body(artifactServices.stream()
                        .map(ArtifactService::findAll)
                        .flatMap(List::stream)
                        .toList());
    }

}
