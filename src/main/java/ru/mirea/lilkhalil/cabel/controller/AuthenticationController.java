package ru.mirea.lilkhalil.cabel.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.dto.AuthRqDto;
import ru.mirea.lilkhalil.cabel.dto.AuthRsDto;
import ru.mirea.lilkhalil.cabel.service.impl.AuthenticationService;

@RestController
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/auth")
    public ResponseEntity<AuthRsDto> authenticate(@RequestBody AuthRqDto requestBody)
    {
        return ResponseEntity.ok()
                .body(authenticationService.authenticate(requestBody.getEmail(), requestBody.getPassword()));
    }

    @GetMapping
    public String foo() {
        return "Hello, World!";
    }

}
