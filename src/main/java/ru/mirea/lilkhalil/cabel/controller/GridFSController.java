package ru.mirea.lilkhalil.cabel.controller;

import java.net.URI;
import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.mirea.lilkhalil.cabel.dto.ArtifactDto;
import ru.mirea.lilkhalil.cabel.service.impl.GridFSService;

import static org.springframework.http.MediaType.parseMediaType;

@RestController
@RequiredArgsConstructor
@RequestMapping("/files")
public class GridFSController {

    private final GridFSService gridFSService;

    @PostMapping
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) {
        String objectId = gridFSService.uploadFile(file);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(objectId).toUri();
        return ResponseEntity.created(location)
                .body(objectId);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> download(@PathVariable("id") String objectId) {
        GridFsResource gridFsResource = gridFSService.downloadFile(objectId);
        return ResponseEntity.ok()
                .contentType(parseMediaType(gridFsResource.getContentType()))
                .body(gridFsResource);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String objectId) {
        gridFSService.deleteFile(objectId);
        return ResponseEntity.noContent().build();
    }

}
