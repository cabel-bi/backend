package ru.mirea.lilkhalil.cabel.controller;

import java.util.Collection;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.dto.RoleDto;
import ru.mirea.lilkhalil.cabel.service.impl.RoleService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/roles")
public class RoleController {

    private final RoleService roleService;

    @GetMapping
    public ResponseEntity<Collection<RoleDto>> findAll() {
        return ResponseEntity.ok()
                .body(roleService.findAll());
    }
}
