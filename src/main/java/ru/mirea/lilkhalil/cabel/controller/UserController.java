package ru.mirea.lilkhalil.cabel.controller;

import java.net.URI;
import java.util.Collection;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.mirea.lilkhalil.cabel.dto.RegRqDto;
import ru.mirea.lilkhalil.cabel.dto.UserDto;
import ru.mirea.lilkhalil.cabel.service.impl.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<Collection<UserDto>> findAll() {
        return ResponseEntity.ok()
                .body(userService.findAll());
    }

    @PostMapping
    public ResponseEntity<UserDto> createUser(@RequestBody RegRqDto requestBody) {
        UserDto userDto = userService.createUser(requestBody);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(userDto.getId()).toUri();
        return ResponseEntity.created(location)
                .body(userDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent()
                .build();
    }

    @GetMapping("/search")
    public ResponseEntity<UserDto> findByEmail(@RequestParam("email") String email) {
        return ResponseEntity.ok()
                .body(userService.findByEmail(email));
    }
}
