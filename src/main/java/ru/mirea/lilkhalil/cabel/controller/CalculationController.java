package ru.mirea.lilkhalil.cabel.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.dto.CalculationRsDto;
import ru.mirea.lilkhalil.cabel.dto.CalculationRqDto;
import ru.mirea.lilkhalil.cabel.registry.CalculationServiceRegistry;
import ru.mirea.lilkhalil.cabel.service.CalculationService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/calculations")
public class CalculationController {

    private final CalculationServiceRegistry serviceRegistry;

    @PostMapping
    public ResponseEntity<List<CalculationRsDto>> calculateData(@RequestBody CalculationRqDto calculationRqDto) {
        String type = calculationRqDto.getType();
        CalculationService service = serviceRegistry.get(type);
        return ResponseEntity.ok()
                .body(service.calculate(calculationRqDto));
    }
}
