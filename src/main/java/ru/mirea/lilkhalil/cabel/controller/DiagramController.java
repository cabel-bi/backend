package ru.mirea.lilkhalil.cabel.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.dto.DiagramDto;
import ru.mirea.lilkhalil.cabel.service.impl.DiagramService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/diagrams")
public class DiagramController {

    private final DiagramService diagramService;

    @PostMapping
    public ResponseEntity<Void> createDiagram(@RequestBody DiagramDto diagramDto, @AuthenticationPrincipal User user) {
        diagramService.createDiagram(diagramDto, user);
        return ResponseEntity.ok()
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDiagram(@PathVariable("id") Long id) {
        diagramService.deleteById(id);
        return ResponseEntity.noContent()
                .build();
    }
}
