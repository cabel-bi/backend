package ru.mirea.lilkhalil.cabel.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.domain.User;
import ru.mirea.lilkhalil.cabel.dto.ChartDto;
import ru.mirea.lilkhalil.cabel.service.impl.ChartService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/charts")
public class ChartController {

    private final ChartService chartService;

    @PostMapping
    public ResponseEntity<Void> createChart(@RequestBody ChartDto chartDto, @AuthenticationPrincipal User user) {
        chartService.createChart(chartDto, user);
        return ResponseEntity.ok()
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteChart(@PathVariable("id") Long id) {
        chartService.deleteById(id);
        return ResponseEntity.noContent()
                .build();
    }
}
