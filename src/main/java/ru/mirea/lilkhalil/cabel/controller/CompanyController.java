package ru.mirea.lilkhalil.cabel.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.domain.Company;
import ru.mirea.lilkhalil.cabel.service.impl.CompanyService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyService companyService;

    @GetMapping
    public List<Company> findAll() {
        return companyService.findAll();
    }
}
