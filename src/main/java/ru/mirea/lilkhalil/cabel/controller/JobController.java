package ru.mirea.lilkhalil.cabel.controller;

import java.util.Collection;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.dto.JobDto;
import ru.mirea.lilkhalil.cabel.service.impl.JobService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/jobs")
public class JobController {

    private final JobService jobService;

    @GetMapping
    public ResponseEntity<Collection<JobDto>> findAll() {
        return ResponseEntity.ok()
                .body(jobService.findAll());
    }
}
