package ru.mirea.lilkhalil.cabel.controller;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mirea.lilkhalil.cabel.dto.AnalysisRqDto;
import ru.mirea.lilkhalil.cabel.dto.TableRowDto;
import ru.mirea.lilkhalil.cabel.registry.AnalysisServiceRegistry;
import ru.mirea.lilkhalil.cabel.service.AnalysisService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/analysis")
public class AnalysisController {

    private final AnalysisServiceRegistry serviceRegistry;

    @PostMapping
    public ResponseEntity<List<TableRowDto>> performAnalysis(@RequestBody AnalysisRqDto analysisRqDto) {
        String type = analysisRqDto.getType();
        AnalysisService analysisService = serviceRegistry.get(type);
        return ResponseEntity.ok()
                .body(analysisService.perform(analysisRqDto));
    }
}
