package ru.mirea.lilkhalil.cabel.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.mirea.lilkhalil.cabel.dto.ErrorDto;

@RestControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorDto> handleRuntimeException(RuntimeException ex) {
        logger.error(ex);
        return ResponseEntity.badRequest()
                .body(new ErrorDto()
                        .setStatus(HttpStatus.BAD_REQUEST)
                        .setMessage(ex.getMessage()));
    }
}
