package ru.mirea.lilkhalil.cabel.filter;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.mirea.lilkhalil.cabel.dto.ErrorDto;
import ru.mirea.lilkhalil.cabel.exception.UserNotFoundException;
import ru.mirea.lilkhalil.cabel.service.impl.JwtService;

/**
 * Указывает, что аннотированный класс является "компонентом". Такие классы
 * рассматриваются как кандидаты на автоматическое обнаружение при использовании
 * конфигурации на основе аннотаций и сканирования путей к классам.
 */
@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    /**
     * Экземпляр класса {@link JwtService}
     */
    private final JwtService jwtService;
    /**
     * Экземпляр класса
     * {@link org.springframework.security.core.userdetails.UserDetailsService}
     */
    private final UserDetailsService userDetailsService;

    private final ObjectMapper objectMapper;

    /**
     * Тот же контракт, что и для {@code doFilter}, но гарантированно
     * вызывается один раз для каждого запроса в одном потоке запросов.
     * See {@code shouldNotFilterAsyncDispatch} for details.
     *
     * @param request     интерфейс для предоставления информации запроса для
     *                    сервлетов HTTP.
     * @param response    интерфейс для предоставления специфичных для HTTP функций
     *                    при отправке ответа
     * @param filterChain объект, предоставляемый контейнером сервлета разработчику,
     *                    дающий представление о цепочке вызовов отфильтрованного
     *                    запроса на ресурс.
     */
    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain) throws ServletException, IOException {
        if (request.getServletPath().contains("/auth")) {
            filterChain.doFilter(request, response);
            return;
        }
        final String authHeader = request.getHeader("Authorization");
        final String jwt;
        final String username;
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }
        jwt = authHeader.substring(7);
        try {
            username = jwtService.extractUsername(jwt);
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userdetails = this.userDetailsService.loadUserByUsername(username);
                if (jwtService.isTokenValid(jwt, userdetails)) {
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                            userdetails,
                            null,
                            userdetails.getAuthorities());
                    authToken.setDetails(
                            new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }
            }
            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException | UserNotFoundException e) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.getWriter().write(objectMapper.writeValueAsString(
                    new ErrorDto()
                            .setStatus(HttpStatus.UNAUTHORIZED)
                            .setMessage(e.getMessage())
            ));
        }
    }
}