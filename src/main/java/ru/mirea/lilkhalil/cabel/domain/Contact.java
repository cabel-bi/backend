package ru.mirea.lilkhalil.cabel.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.experimental.Accessors;

@Entity
@Table(name = "contacts", schema = "integration")
@Data
@Accessors(chain = true)
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String email;
    private String country;
    private String region;
    private String district;
    @Column(nullable = false)
    private String city;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;
}
