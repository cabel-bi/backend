package ru.mirea.lilkhalil.cabel.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.experimental.Accessors;

@Entity
@Table(name = "companies", schema = "integration")
@Data
@Accessors(chain = true)
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;
    private String country;
    private String region;
    private String district;
    @Column(nullable = false)
    private String city;

    @Override
    public String toString() {
        return name;
    }
}
