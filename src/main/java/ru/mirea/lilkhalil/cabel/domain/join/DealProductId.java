package ru.mirea.lilkhalil.cabel.domain.join;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@Accessors(chain = true)
public class DealProductId implements Serializable {
    @Column(name = "deal_id", nullable = false)
    private Long dealId;
    @Column(name = "product_id", nullable = false)
    private Integer productId;
}
