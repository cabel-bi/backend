package ru.mirea.lilkhalil.cabel.domain.join;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@Accessors(chain = true)
public class UserRoleId implements Serializable {
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "role_id", nullable = false)
    private Integer roleId;
}
