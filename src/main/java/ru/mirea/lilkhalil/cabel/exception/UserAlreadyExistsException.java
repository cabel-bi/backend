package ru.mirea.lilkhalil.cabel.exception;

public class UserAlreadyExistsException extends RuntimeException {

    private static final String message = "User with email: %s already exists!";

    public UserAlreadyExistsException(String email) {
        super(message.formatted(email));
    }

}
