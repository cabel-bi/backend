package ru.mirea.lilkhalil.cabel.exception;

public class UserNotFoundException extends RuntimeException {

    private static final String message = "User with email %s wasn't found!";

    public UserNotFoundException(String email) {
        super(message.formatted(email));
    }

}
