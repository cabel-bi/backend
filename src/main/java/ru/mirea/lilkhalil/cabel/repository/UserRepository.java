package ru.mirea.lilkhalil.cabel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
