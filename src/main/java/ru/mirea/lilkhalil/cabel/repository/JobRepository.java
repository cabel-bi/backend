package ru.mirea.lilkhalil.cabel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Job;

public interface JobRepository extends JpaRepository<Job, Integer> {

}
