package ru.mirea.lilkhalil.cabel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
