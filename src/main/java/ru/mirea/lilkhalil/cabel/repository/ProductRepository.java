package ru.mirea.lilkhalil.cabel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
