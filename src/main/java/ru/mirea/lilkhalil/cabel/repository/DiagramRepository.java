package ru.mirea.lilkhalil.cabel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Diagram;

public interface DiagramRepository extends JpaRepository<Diagram, Long> {
}
