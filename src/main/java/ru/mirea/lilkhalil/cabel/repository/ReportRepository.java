package ru.mirea.lilkhalil.cabel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Report;

public interface ReportRepository extends JpaRepository<Report, Long> {
}
