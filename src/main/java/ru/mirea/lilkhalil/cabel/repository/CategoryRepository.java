package ru.mirea.lilkhalil.cabel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Category;
import ru.mirea.lilkhalil.cabel.domain.Product;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
