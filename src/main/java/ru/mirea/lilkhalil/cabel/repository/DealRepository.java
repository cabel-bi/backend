package ru.mirea.lilkhalil.cabel.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirea.lilkhalil.cabel.domain.Category;
import ru.mirea.lilkhalil.cabel.domain.Company;
import ru.mirea.lilkhalil.cabel.domain.Deal;
import ru.mirea.lilkhalil.cabel.domain.Employee;

public interface DealRepository extends JpaRepository<Deal, Long> {
    List<Deal> findByCreationDateGreaterThanEqualAndClosingDateLessThanEqual(LocalDateTime startDate, LocalDateTime endDate);
}
